using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TendsOff : MonoBehaviour
{
    [SerializeField] GameObject tend;
    Material sharedMat;

    private void Awake()
    {
        sharedMat = tend.GetComponent<MeshRenderer>().sharedMaterial;
        sharedMat.color = new Color(1, 1, 1, 1);
    }

    float value = 5;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            //DOTween.To( () => value, x => value = x, 1, 0.5f);
            sharedMat.DOColor(new Color(1, 1, 1, 0), 3).SetEase(Ease.OutQuint);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            sharedMat.DOColor(new Color(1, 1, 1, 1), 3).SetEase(Ease.OutQuint);
        }
    }
}
