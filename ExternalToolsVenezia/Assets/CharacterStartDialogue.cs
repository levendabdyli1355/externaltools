using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStartDialogue : MonoBehaviour
{
    public GameObject NPCCamera;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!collision.CompareTag("Player")){ return; }

        collision.GetComponent<CharacterController2D>().myCamera.SetActive(false);
        NPCCamera.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player")) { return; }

        collision.GetComponent<CharacterController2D>().myCamera.SetActive(true);
        NPCCamera.SetActive(false);
    }
}
